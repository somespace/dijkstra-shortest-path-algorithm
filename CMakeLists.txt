cmake_minimum_required(VERSION 3.5)

project(advanced_dijsktra_shortest_path LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(advanced_dijsktra_shortest_path main.cpp graph.cpp shortest_path.cpp)
