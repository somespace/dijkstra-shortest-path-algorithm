#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <limits>
#include <vector>
#include <list>

using namespace std;

// constant value of "infinity" for float type
const float INF = numeric_limits<float>::infinity();

class Graph
{
public:
    // overloaded constructors generate graph with given/default parameters
    Graph(int size = 25, float range = 9.9f, float density = 0.2f);
    ~Graph(); // destructor

    const int V; // amount of nodes in the graph

    inline float get_density() { return edge_density; }
    inline void subtract_unvisited() { --unvisited; }
    // reset src_node_dist, visited and parents before next calculation
    void reset_path_calculation();
    // print the matrix of the graph to the console
    void print_graph();

    // befriended class Shortest_Path can access private members of Graph class
    friend class Shortest_Path;
private:
    // class members variables
    // graph parameters
    int unvisited = V; // count of unvisited nodes
    float distance_range; // range within 1.0 and n for random edge value
    float edge_density; // the average number of edges per node

    // dynamic connectivity matrix
    vector<vector<float>> graph_matrix;

    // store minimal src -> node distances (initialised to INF)
    vector<float> src_node_dist;
    vector<bool> visited;
    vector<int> parents; // store shortest path parents to each element

    // class member functions
    void generate_graph();
};

#endif // GRAPH_H
