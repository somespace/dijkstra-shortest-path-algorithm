#ifndef SHORTEST_PATH_H
#define SHORTEST_PATH_H

#include <iostream>
#include <list>
#include "graph.h"

using namespace std;

class Shortest_Path
{
public:
    Shortest_Path(Graph g) : graph(g) {}
    ~Shortest_Path(); // destructor

    // getter to get graph outside the class
    inline Graph get_graph() { return graph; }

    // helper to get node with min distance src->node
    int min_node_index();

    // clear shortest path before a new src->dest calculation
    inline void reset_shortest_path() { shortest_path.clear(); }

    // calculate the shortest path for src->dest nodes
    // & return shortest path src->dest value
    float calculate_path(int src, int dest);
    void fill_shortest_path();

    // print the shortest path, node by node src to dest
    void print_shortest_path();

private:
    Graph graph; // holds the graph to calculate shortest path for
    list<int> shortest_path; // ADT to store shortest path in list form
};

#endif // SHORTEST_PATH_H
