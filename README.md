Dijkstra Shortest Path algorithm (own implementation):

Reference: https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

Ways to open the project:

1. Open the file named _CMakeLists.txt_ in QT Creator IDE.
2. Unzip archive and navigate to the project folder in terminal, 
   then run this command: cmake CMakeLists.txt && make
